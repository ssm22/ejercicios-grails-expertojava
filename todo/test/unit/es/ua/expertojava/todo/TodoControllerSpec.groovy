package es.ua.expertojava.todo

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.*
import spock.lang.*
import todo.TodoService

@TestFor(TodoController)
@Mock(Todo)
class TodoControllerSpec extends Specification {

    def todoService = new TodoService()
    def springSecurityService = Mock(SpringSecurityService)
    def usuario1

    def setup()
    {
       usuario1=new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "Usuario1", surnames: "Usuario1", email: "usuario1@ua.es").save();
        controller.todoService = todoService
        controller.springSecurityService = springSecurityService//o todoService.springSecurityService
        springSecurityService.isLoggedIn() >> true
        springSecurityService.getCurrentUser() >> usuario1



        usuario1.getUsername()>> "usuario1"

    }
    def populateValidParams(params) {
        assert params != null

        params["title"] = 'pasear al perro'
        params["date"] = new Date()
        params["completed"] = true
        params["user"] = usuario1
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.todoInstanceList
            model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.todoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def todo = new Todo()
            todo.validate()
            controller.save(todo)

        then:"The create view is rendered again with the correct model"
            model.todoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params)

            controller.save(todo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/api/todos/1'
            controller.flash.message != null
            Todo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.show(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.edit(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/api/todos'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def todo = new Todo()
            todo.validate()
            controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params).save(flush: true)
            controller.update(todo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/api/todos/$todo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/api/todos'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def todo = new Todo(params).save(flush: true)

        then:"It exists"
            Todo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(todo)

        then:"The instance is deleted"
            Todo.count() == 0
            response.redirectedUrl == '/api/todos'
            flash.message != null
    }

    void "Test method listNextTodos correct"() {

        when:"listNextTodos is executed"
        def todo = new Todo(title: "Pintar cocina", date: new Date() + 1, completed: true).save(flush: true)
        controller.listNextTodos(3)

        then:"one instance find"
        model.todoInstanceCount == 1
        view == "index"
    }

    void "Test method resultListByCategory() correct"() {

        when:"resultListByCategory is executed"
        def categoryHome = new Category(name: "Hogar").save(flush: true)
        controller.resultListByCategory()

        then:"one instance find"
        view== "resultListByCategory"
    }



}
