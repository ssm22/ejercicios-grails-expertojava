package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "la fecha de recordatorio no puede ser posterior a la fecha de creacion"() {
        given:
        def c1 = new Todo(title:"prueba",date:new Date(),reminderDate:new Date()+10)
        when:
        c1.validate()
        then:
        c1?.errors['reminderDate']

    }

    void "la fecha de recordatorio debe ser posterior a la fecha de creacion"() {
        given:
        def c1 = new Todo(title:"prueba",date:new Date(),reminderDate:new Date()-10)
        when:
        c1.validate()
        then:
        !c1?.errors['reminderDate']

    }
}
