package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Tag)
class TagSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "El nombre de la etiqueta esta correcto"() {
        given:
        def c1 = new Tag(name:"algo", colorTag: "#009966")
        when:
        c1.validate()
        then:
        !c1?.errors['name']
    }
}
