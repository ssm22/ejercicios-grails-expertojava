package todo

import es.ua.expertojava.todo.Todo
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NO_CONTENT


class TodoService {

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }

    def delete(Todo todoInstance) {

        todoInstance.tags.each{tag-> todoInstance.removeFromTags(tag)}

        todoInstance.delete flush:true

    }
    def saveTodo(Todo todoInstance){

        if(todoInstance.completed==true){

            todoInstance.dateDone=new Date()
        }
    }

    def lastTodosDone(Integer hours){

        Date hourEnd = new Date(System.currentTimeMillis())
        Date hourFirst = new Date(System.currentTimeMillis()- hours*60*60*1000)
        def todosFilter= Todo.findAllByDateDoneBetween(hourFirst,hourEnd)

    }
}
