package es.ua.expertojava.todo

class TodoTagLib {
    static namespace = 'todo'

    def printIconFromBoolean={ attrs, body ->
        def mkp = new groovy.xml.MarkupBuilder(out)
        if (attrs.value) {
            mkp.img(src: "${assetPath(src:"good.png")}")

        }else{
            mkp.img(src:"${assetPath(src:"notgood.png")}")
            }
        }
    }

