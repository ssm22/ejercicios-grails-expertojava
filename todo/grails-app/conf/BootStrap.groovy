import es.ua.expertojava.todo.*


class BootStrap {

        def init = { servletContext ->

            Role roleAdmin = new Role(authority: "ROLE_ADMIN").save();
            Role roleBasic = new Role(authority: "ROLE_BASIC").save();

            User admin = new User(username: "admin", password: "admin", confirmPassword: "admin", name: "Admin", surnames: "Admin", email: "admin@ua.es").save();
            User usuario1 = new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "Usuario1", surnames: "Usuario1", email: "usuario1@ua.es").save();
            User usuario2 = new User(username: "usuario2", password: "usuario2", confirmPassword: "usuario2", name: "Usuario2", surnames: "Usuario2", email: "usuario2@ua.es").save();

            PersonRole.create(admin, roleAdmin).save();
            PersonRole.create(usuario1, roleBasic).save();
            PersonRole.create(usuario2, roleBasic).save();

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil", colorTag: "#009966").save()
            def tagDifficult = new Tag(name: "Difícil", colorTag: "#FF0033").save()
            def tagArt = new Tag(name: "Arte", colorTag: "#CCFF66").save()
            def tagRoutine = new Tag(name: "Rutina", colorTag: "#009966").save()
            def tagKitchen = new Tag(name: "Cocina", colorTag: "#996699").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, completed: true)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, completed: false)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, completed: false)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), completed: true)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()

            usuario1.addToTodos(todoCollectPost)
            usuario1.addToTodos(todoPaintKitchen)
            usuario2.addToTodos(todoBakeCake)
            usuario2.addToTodos(todoWriteUnitTests)

        }


    def destroy = { }
}
