package es.ua.expertojava.todo

class Tag {

    String name
    String colorTag

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:true, unique:true)
        colorTag shared:"rgbcolor"
    }

    String toString(){
        name
    }
}
