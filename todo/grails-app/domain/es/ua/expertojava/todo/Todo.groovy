package es.ua.expertojava.todo

class Todo {

    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    boolean completed
    User user
    Date dateCreated
    Date lastUpdated
    Date dateDone


    static hasMany = [tags:Tag]
    static belongsTo = [Tag]


    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true,validator: { val, obj ->
            if (val && obj.date) {
                return val.before(obj?.date)
            }
            return true
        })
        url(nullable:true, url:true)
        category(nullable:true)
        completed(nullable:true)
        dateDone(nullable:true)
        user(nullable:true)
    }

    String toString(){
        "title"
    }
}
