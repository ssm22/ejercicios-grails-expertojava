

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.listCategory.label"  /></title>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <a href="#listCategoryTodo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
        </ul>
</div>
<div id="listCategoryTodo" class="content scaffold-list" role="main">
    <h1><g:message code="default.listCategory.label"  /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form controller="todo" action="resultListByCategory">
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="name" title="${message(code: 'category.name.label', default: 'Name')}" />
            <th>Seleccione Categorias</th>
        </tr>
        </thead>

        <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${categoryInstance.id}">${fieldValue(bean: categoryInstance, field: "name")}</g:link></td>
                <td>

                    <g:checkBox name="categoriasSeleccionadas" value="${categoryInstance.id}" checked="false"></g:checkBox>

                </td>

            </tr>
        </g:each>

        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${categoryInstanceCount ?: 0}" />
    </div>
    <fieldset class="buttons">
        <g:submitButton  name="find" action="resultListByCategory" class="save"  value="${message(code: 'default.button.result.label', default: 'Find')}"/>
    </fieldset>
        </g:form>
    </div>

</body>
</html>