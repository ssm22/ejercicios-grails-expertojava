
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.listResult.label"/></title>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <a href="#resultListCategory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="listCategoryTodo" class="content scaffold-list" role="main">
    <h1><g:message code="default.listCategory.label"  /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            <g:sortableColumn property="name" title="${message(code: 'todo.title.label', default: 'Name')}" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${todoInstanceList}" status="i" var="categoryTodoInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td class="property-value"><g:link controller="todo" action="show" id="${categoryTodoInstance.id}">${categoryTodoInstance.title}</g:link></td>


            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${categoryTodoInstanceCount ?: 0}" />
    </div>



</div>
</body>
</html>