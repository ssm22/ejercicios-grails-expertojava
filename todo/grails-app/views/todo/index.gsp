
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li><g:link action="listByCategory"><g:message code="default.listCategory.label" args="${todoInstanceList.size()}" /></g:link></li>
			</ul>
		</div>
		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<p>${username}</p>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />

						<g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />

						<g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />

						<g:sortableColumn property="completed" title="${message(code: 'completed.url.label', default: 'Completed')}" />

						<th><g:message code="todo.category.label" default="Category" /></th>
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${todoInstanceList}" status="i" var="todoInstance">

					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>
					

						<td><g:formatDate date="${todoInstance.date}" /></td>

					
						<td>${fieldValue(bean: todoInstance, field: "url")}</td>


                        <td><todo:printIconFromBoolean value="${todoInstance.completed}"/>

						</td>

						<td>${fieldValue(bean: todoInstance, field: "category")}</td>

						<td><g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
							<div class="buttons listaTodo">

								<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
								<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

							</div>
						</g:form></td>
					
					</tr>

				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
