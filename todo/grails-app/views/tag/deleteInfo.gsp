
<%@ page import="es.ua.expertojava.todo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.delete.label" args="[entityName]" /></title>
</head>

<body>

<a href="#show-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div class="delete-tag">
    <fieldset>
    <h2>¿Estas seguro que deseas borrar ${tagInstance.name} ?</h2>
    <asset:image src="warning.png" style="width: 100px"/>
</fieldset>
</div>
<g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
    <fieldset class="buttons">
        <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
    </fieldset>
</g:form>

</body>
</html>