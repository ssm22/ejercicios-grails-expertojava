<div id="header">
<div id="menu">
    <nobr>
        <sec:ifLoggedIn>
            <sec:username/>
            <form id="logout"  name="logout" method="POST" action="${createLink(controller:'logout') }">
            <input type='submit' id="submit" value="logout"/>
            </form>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:link controller="login" action="auth">Login</g:link>
        </sec:ifNotLoggedIn>
    </nobr>
</div>
</div>