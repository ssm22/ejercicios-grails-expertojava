package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def todoService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        //Añadiendo usuario a las tareas
        User myuser=springSecurityService.isLoggedIn() ?
                springSecurityService.getCurrentUser() :
                null

        //def username= user.username
        //User myuser=User.findByUsername(username)
        if(myuser.getUsername()=="admin"){

            respond Todo.list(params), model:[todoInstanceCount: Todo.count()]

        }
        else {
            def listaAux = []
            listaAux = Todo.findAllByUser(myuser)
            if (listaAux == null) listaAux = []

            respond listaAux , model:[todoInstanceCount: listaAux.size()],
                view: "index"
            }
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {

        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {


        if (todoInstance != null) {
            //Añadiendo usuario a las tareas
            User myuser = springSecurityService.isLoggedIn() ?
                    springSecurityService.getCurrentUser() :
                    null

            // username = user.username
            //User myuser = User.findByUsername(username)
            myuser.addToTodos(todoInstance)
            todoInstance.user= new User()
            todoInstance.user= myuser
        }

        if (todoInstance == null) {
            notFound()
            return
        }



        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoService.saveTodo(todoInstance)

        todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.delete(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount: todoService.countNextTodos(days)],
                view:"index"
    }

def listByCategory(Integer max){

    params.max = Math.min(max ?: 10, 100)
    respond Category.list(params), model:[categoryInstanceCount: Category.count()]

}
@Transactional
    def resultListByCategory(){

        def IdcategoriasSelec= params.list('categoriasSeleccionadas')

        def resultListaux=Category.findAllByIdInList(IdcategoriasSelec)
        def resultList= []

        resultList= Todo.findAllByCategoryInList(resultListaux)


    respond resultList , model:[todoInstanceCount: resultList.size()],
            view: "resultListByCategory"


    }
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }


}
