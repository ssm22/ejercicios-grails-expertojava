
def factorial(Integer numero) { 
    (numero == 0 || numero == 1) ? numero : numero * factorial(numero - 1)
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Recorremos una lista de numeros mostrando el resultado de la operación factorial*/

def arrayNumeros=[1,2,3,4,5,6,7,8,9,10]
(0..arrayNumeros.size()-1).each{println "Factorial de "+(it+1)+" = "+ factorial(arrayNumeros[it])}

/*Creamos Clousures Ayer y Mañana y mostramos sus fechas*/
//def fecha = new Date().parse("d/M/yyyy","28/5/2008")

def Ayer={fechaDada-> date= new Date().parse("d/M/yyyy",fechaDada)
                      date= date-1
                     return date.format("dd/MM/yyyy")}
def  Mañana={fechaDada-> date= new Date().parse("d/M/yyyy",fechaDada)
                      date= date+1
                     return date.format("dd/MM/yyyy")}
                      
def listaFechas=['03/02/2012','13/01/2015','10/12/2002','31/01/2012']
(0..listaFechas.size()-1).each{println "Fecha anterior: "+Ayer(listaFechas[it])+" y Fecha posterior: "+ Mañana(listaFechas[it])}

