BigDecimal.metaClass.moneda = {String local->
     if(local=="en_EN")"£${delegate}"
     else if(local=="en_US") "\$${delegate}"
     else if(local=="es_ES") "${delegate}€"
}
Integer.metaClass.moneda = {String local->
     if(local=="en_EN")"£${delegate}"
     else if(local=="en_US") "\$${delegate}"
     else if(local=="es_ES") "${delegate}€"
}
Float.metaClass.moneda = {String local->
     if(local=="en_EN")"£${delegate}"
     else if(local=="en_US") "\$${delegate}"
     else if(local=="es_ES") "${delegate}€"
}
Double.metaClass.moneda = {String local->
     if(local=="en_EN")"£${delegate}"
     else if(local=="en_US") "\$${delegate}"
     else if(local=="es_ES") "${delegate}€"
}
assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"