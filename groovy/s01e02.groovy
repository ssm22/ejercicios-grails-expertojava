class Libro {
    String nombre
    Integer anyo
    String autor
    String editorial
    
  String getAutor(){
              
         String[] autorAux=autor.split(",")
        return (autorAux[1]+" "+ autorAux[0]).trim() 
     }
}
def l1= new Libro(nombre:'La colmena', anyo:1951,autor:'Cela Trulock, Camilo José')
def l2= new Libro(nombre:'La galatea', anyo:1585,autor:'de Cervantes Saavedra, Miguel')
def l3= new Libro(nombre:'La dorotea', anyo:1632,autor:'Lope de Vega y Carpio, Félix Arturo')

l1.editorial='Anaya'
l2.editorial='Planeta'
l3.editorial='Santillana'


assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'


assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'