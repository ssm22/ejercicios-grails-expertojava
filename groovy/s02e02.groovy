System.in.withReader {
  print 'Introduzca primer operador: '
  op1 = it.readLine().tokenize(' ')
  print 'Introduzca segundo operador: '
  op2 = it.readLine().tokenize(' ')
  print 'Introduzca operacion (sum,res,mul o div): '
  operation = it.readLine().tokenize(' ')
  def valor= new Calculadora(operando1:op1[0].toInteger(),operando2:op2[0].toInteger())
  if(operation[0]=='sum'){
  result = valor.suma()
  }
  else if(operation[0]=='res'){
  result = valor.resta()
  }
  else if(operation[0]=='mul'){
  result = valor.multiplicacion()
  }
  else if(operation[0]=='div'){
  if(op2[0].toInteger()==0) result = 'no se puede dividir por cero'
  else result = valor.division()
  
  }
  else result='Operacion no identificada'
  println('Resultado = '+result)
}
class Calculadora{

Integer operando1,operando2

def suma(){
    return operando1 + operando2
}
def resta(){
    return operando1 - operando2
}
def multiplicacion (){
    return operando1 * operando2
}
def division(){
    return operando1 / operando2
}   
}